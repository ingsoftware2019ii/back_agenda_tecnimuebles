const contratoController= {};
const mysqlConnection = require('../database/database');

contratoController.SP_contrato_insert_update = function(req, res){

    const {id_contrato, fecha, dias_entrega, condicion_pago, id_cliente} = req.body;
    const query = `call SP_contrato_insert_update(?, ?, ?, ?, ?)`;
    mysqlConnection.query(query, [id_contrato, fecha, dias_entrega, condicion_pago, id_cliente], (err, contrato) => {
        if(!err){
            res.json({
                status: 'success',
                code:200,
                message: contrato[0][0].var_mensaje
            });
        }
        else{
            res.json(err);
        }
    })
}

contratoController.SP_contrato_list = function(req, res){
    const id_contrato = req.params.id;
    const query= `call SP_contrato_list(?)`;
    mysqlConnection.query(query,[id_contrato],(err,contrato)=>{ 
        if(!err){
            res.json({
                status:'success',
                code:200,
                contrato:contrato[0]
            })
        }
        else{
            res.json(err);
        }
    })
}  

module.exports = contratoController;
