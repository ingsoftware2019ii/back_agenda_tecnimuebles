const administradorController= {};
const jwt = require('../middlewares/jwt');
const mailer = require('./mailer.controller');
var generator = require('generate-password');
const mysqlConnection = require('../database/database');

administradorController.SP_admin_login = function(req, res){
    const {correo,password} = req.body
    const query= `call SP_admin_login(?)`;
    mysqlConnection.query(query, [correo], (err, admin) => {
        if(!err){
            if(admin[0][0]!==undefined){
                if(admin[0][0].estado===1){
                    if(password == admin[0][0].password) {
                        token = jwt.crearToken(admin[0][0]);
                        res.json({
                            status: 'success',
                            code:200,
                            usuario:admin[0],
                            token:token
                        });
                    }
                    else{
                        res.json({
                            status: 'invalid',
                            code:400,
                            message:"Contraseña incorrecta"
                        });
                    }
                }
                else {
                    res.json({
                        status: 'invalid',
                        code:300,
                        message:"El usuario se encuentra deshabilitado"
                    }); 
                }
            }
            else{
                res.json({
                    status: 'empty',
                    code:300,
                    message:"El correo ingresado no existe"
                });
            }
        }
        else {
            res.json(err);
        }
    })
}

administradorController.SP_admin_insert_update = function(req, res){
    const {id_admin, id_tipo_admin, nombre, apepat, apemat, telefono, correo,dni, estado} = req.body;
    const query = `call SP_admin_insert_update(?, ?, ?, ?, ?, ?, ?, ?, ?,?)`;
    const codigo = generator.generate({
        length: 4,
        numbers: true
    });
    mysqlConnection.query(query, [id_admin, id_tipo_admin, nombre, apepat, apemat, telefono, correo, codigo, dni, estado], (err, admin) => {
        if(!err){
            if(admin[0][0].var_estado){
                res.json({
                    status: 'success',
                    code:200,
                    message: admin[0][0].var_mensaje
                });
                if(admin[0][0].var_verificacion){
                    var data = {
                        correo:correo,
                        password:codigo
                    }
                    mailer.sendPassword(data,1 , "ADMINISTRADOR");
                }
            }
            else {
                res.json({
                    status: 'mistake',
                    code:400,
                    message: admin[0][0].var_mensaje
                });
            }
        }
    });
}

administradorController.SP_admin_update = function(req, res){
    const id_admin = req.user.id_admin;
    const {nombre, apepat, apemat, telefono} = req.body;
    const query = `call SP_admin_update(?, ?, ?, ?, ?)`;
    mysqlConnection.query(query, [id_admin, nombre, apepat, apemat, telefono], (err, rows) =>{
        if(!err){
            res.json({
                status: 'success',
                code:200,
                message: "Datos modificados con exito"
            });
        }
        else{
            res.json(err);
        }
    })
}

administradorController.SP_admin_update_estado = function(req, res){
    const {id_admin, estado} = req.body;
    console.log(req.body);
    const query = `call SP_admin_update_estado(?, ?)`;
    mysqlConnection.query(query, [id_admin, estado], (err, rows) =>{
        if(!err){
            
            res.json({
                status: 'success',
                code:200,
                message: "El estado se cambio correctamente"
            });
        }
        else{
            res.json(err);
        }
    })
}

administradorController.SP_admin_update_password = function(req, res){
    const correo  = req.user.correo;
    const {passActual, passNuevo} = req.body;
    const query = `call SP_admin_login(?)`;
    mysqlConnection.query(query, [correo], (err, admin) => {
        if(!err){
            if(passActual == admin[0][0].password){
                const query2 = `call SP_admin_update_password(?, ?)`;
                mysqlConnection.query(query2, [correo, passNuevo], function(err, rows){
                    if(!err){
                        res.json({
                            status: 'success',
                            code:200,
                            message: "Cambio de contraseña exitoso"
                        });
                    }else {
                        res.send(err);
                    }
                });
            }
            else{
                res.json({
                    status: 'invalid',
                    code:400,
                    message:"Contraseña incorrecta"
                });
            }
        }
        else{
            res.json(err);
        }
    })
}

administradorController.SP_admin_send_code_password = function(req, res){
    const correo = req.body.correo;
    const query = `call SP_admin_send_code_password(?, ?)`;
    const codigo = generator.generate({
        length: 4,
        numbers: true
    });
    mysqlConnection.query(query, [correo,codigo], (err, rows) => {
        if(!err){   
            if(rows[0][0].flag_envio == 1){
                var data = {
                    correo:correo,
                    password:codigo
                }
                mailer.sendPassword(data,0,"ADMINISTRADOR");
                res.json({
                    status: 'success',
                    code:200,
                    message:rows[0].var_mensaje,
                });
            }else{
                res.json({
                    status: 'invalid',
                    code:400,
                    message:rows[0].var_mensaje,
                });
            }
        }else{
            res.json(err);
        }      
    });
}

administradorController.SP_admin_reset_password = function( req , res ){
    const {correo, codigo, password } = req.body
    const query = `call SP_admin_reset_password(?,?,?)`;
    mysqlConnection.query(query, [correo,codigo,password], (err, rows) => {
        if(!err){
            if(rows[0][0].var_flag == 1){
                var data = {
                    correo: correo,
                    password: password
                }
                mailer.sendPassword(data,1, "ADMINISTRADOR");
                res.json({
                    status: 'success',
                    code:200,
                    message:rows[0][0].var_respuesta
                });
            }
            else{
                res.json({
                    status: 'invalid',
                    code:400,
                    message:rows[0][0].var_respuesta
                });
            }
        }
        else{
            res.json(err);
        }
    });
};


administradorController.SP_admin_list = function(req, res){
    const id_admin = req.params.id;
    const query= `call SP_admin_list(?)`;
    mysqlConnection.query(query,[id_admin],(err,admin)=>{ 
        if(!err){
            res.json({
                status:'success',
                code:200,
                admin:admin[0]
            })
        }
        else{
            res.json(err);
        }
    })
}  


administradorController.SP_admin_list_datos_propios = function(req, res){
    const id_admin = req.user.id_admin;
    const query = `call SP_admin_list(?)`;
    mysqlConnection.query(query,[id_admin],(err,admin)=>{ 
        if(!err){
            res.json({
                status:'success',
                code:200,
                admin:admin[0]
            })
        }
        else{
            res.json(err);
        }
    })
}

administradorController.SP_admin_tipos_list = function(req, res){
    const query = `call SP_admin_tipos_list()`;
    mysqlConnection.query(query,(err,tipos)=>{
        if(!err){
            res.json({
                status:'success',
                code:200,
                tipos:tipos[0]
            })
        }
        else{
            res.json(err);
        }
    });
}

administradorController.SP_reporte_general = function(req,res){
    const query1 = `call SP_reporte_general()`;
    const query2 = `call SP_reporte_dia()`;
    const query3 = `call SP_reporte_mes()`;
    const query4 = `call SP_reporte_y()`;
    mysqlConnection.query(query1,(err, reporte)=>{
        if(!err){
            mysqlConnection.query(query2,[], (err, reporte_d)=>{
                if(!err){
                    mysqlConnection.query(query3,[], (err, reporte_m)=>{
                        if(!err){
                            mysqlConnection.query(query4,[], (err, reporte_y)=>{
                                if(!err){
                                    res.json({
                                        status: 'success',
                                        code: 200,
                                        reporte: reporte[0][0],
                                        reporte_d : reporte_d[0],
                                        reporte_m: reporte_m[0],
                                        reporte_y: reporte_y[0]
                                    })
                                }
                                else{
                                    res.json(err);
                                }
                            })
                        }
                        else{
                            res.json(err);
                        }
                    })
                }
                else{
                    res.json(err);
                }
            })
        }
        else{
            res.json(err);
        }
    })
}

module.exports = administradorController;
