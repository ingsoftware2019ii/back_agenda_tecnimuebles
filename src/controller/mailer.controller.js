﻿const nodemailer = require("nodemailer");
const mailerController = {};

mailerController.sendPassword = function (data,flag,app) {
    let transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true, 
        auth: {
          user: 'gruposoftware2019.ii@gmail.com', 
          pass: 'softwareagenda' 
        }
    });
    transporter.sendMail({
        from: 'gruposoftware2019.ii@gmail.com', 
        to:data.correo,
        subject: 'Tecnimuebles', 
        text: '', 
        html: bodyEmail(data,flag,app)
    });
};


function bodyEmail(dato,tipo,app) {
   
    var body = ''
    if(tipo==1){     
    body = `
    <table class="contenedor" border="0" style="width: 60%; margin:auto;">
    <tr>
        <td style="background: #ed4b5a; border-radius: 22px 22px 0px 0px;">
            <div class="header" style="background: #ed4b5a; text-align: center;padding: 5% 0;width: 90%; margin: auto">
                <a href="http://localhost:4200/" style="text-decoration: none;">
                    <span class="titulo"
                        style="color: #fff; font-size: 40px; font-family: ‘Lucida Sans’, ‘Lucida Sans Regular’, ‘Lucida Grande’, ‘Lucida Sans Unicode’, Geneva, Verdana, sans-serif;">
                        ¡BIENVENIDO A TECNIMUEBLES `+app+`!
                    </span>
                </a>
            </div>
        </td>
    </tr>
    <tr class="cuerpo"
        style="background: #F2F3F4; font-family: ‘Lucida Sans’, ‘Lucida Sans Regular’, ‘Lucida Grande’, ‘Lucida Sans Unicode’, Geneva, Verdana, sans-serif;">
        <td>
            <div class="cuerpo2" style="width: 90%;margin: auto;color: #000;">
                <br><br>
                <div class="titulo2"
                    style="font-size: 20px; text-align: center; font-family: ‘Lucida Sans’, ‘Lucida Sans Regular’, ‘Lucida Grande’, ‘Lucida Sans Unicode’, Geneva, Verdana, sans-serif;">
                    <b> <span style="color: #000;"> Accede a la aplicacion con las siguientes credenciales:</span></b> <br><br>
                </div>
              
                <table style="color:#000;padding-left: 10%;font-size: 16px;">
                    <tr>
                        <td><b>Usuario</b></td>
                        <td><b>: </b>`+ dato.correo+ `</td>
                    </tr>
                    <tr>
                        <td><b>Contraseña</b></td>
                        <td><b>: </b>`+dato.password+`</td>
                    </tr>
                    
                </table>
              <p style="text-align: center;color: #000;">
                *Importate: te recomendamos que cambies tu contraseña la primera vez que accedas a la aplicacion
              </p>
                <br>
                <br>
                <br>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="footer"
                style="background: #ed4b5a; text-align: center; height: 100%; padding-top: 10px; padding-bottom: 10px; color: #fff; font-size: 12px; border-radius: 0px 0px 10px 10px; font-family: ‘Lucida Sans’, ‘Lucida Sans Regular’, ‘Lucida Grande’, ‘Lucida Sans Unicode’, Geneva, Verdana, sans-serif;">
                <span>© 2019 TECNIMUEBLES. Todos los derechos reservados.</span>
            </div>
        </td>
    </tr>
</table>
    `
    }else{
             
    body = `
    <table class="contenedor" border="0" style="width: 60%; margin:auto;">
    <tr>
        <td style="background: #ed4b5a; border-radius: 22px 22px 0px 0px;">
            <div class="header" style="background: #ed4b5a; text-align: center;padding: 5% 0;width: 90%; margin: auto">
                <a href="http://localhost:4200/" style="text-decoration: none;">
                    <span class="titulo"
                        style="color: #fff; font-size: 40px; font-family: ‘Lucida Sans’, ‘Lucida Sans Regular’, ‘Lucida Grande’, ‘Lucida Sans Unicode’, Geneva, Verdana, sans-serif;">
                        ¡ TECNIMUEBLES`+app+`!</span>
                </a>
            </div>
        </td>
    </tr>
    <tr class="cuerpo"
        style="background: #F2F3F4; font-family: ‘Lucida Sans’, ‘Lucida Sans Regular’, ‘Lucida Grande’, ‘Lucida Sans Unicode’, Geneva, Verdana, sans-serif;">
        <td>
            <div class="cuerpo2" style="width: 90%;margin: auto;color: #000;">
                <br><br>
                <div class="titulo2"
                    style="font-size: 20px; text-align: center; font-family: ‘Lucida Sans’, ‘Lucida Sans Regular’, ‘Lucida Grande’, ‘Lucida Sans Unicode’, Geneva, Verdana, sans-serif;">
                    <b> <span> Solicitaste restablecer tu contraseña, usa el siguiente código para acceder temporalmente y luego  configura una nueva contraseña.</span></b> <br><br>
                </div>
              
                <table style="color:#000;padding-left: 10%;font-size: 16px;">
                    <tr>
                        <td><b>Acceso Temporal</b></td>
                        <td><b>:</b> `+ dato.password+ `</td>
                    </tr>
                    <tr>
                        <td><b>Usuario</b></td>
                        <td><b>:</b> `+dato.correo+`</td>
                    </tr>
                    
                </table>
              <p style="text-align: center;color: #000;">
                *Importate: te recomendamos que cambies tu contraseña la primera vez que accedas a la aplicacion.
              </p>
                <br>
                <br>
                <br>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="footer"
                style="background: #ed4b5a; text-align: center; height: 100%; padding-top: 10px; padding-bottom: 10px; color: #fff; font-size: 12px; border-radius: 0px 0px 10px 10px; font-family: ‘Lucida Sans’, ‘Lucida Sans Regular’, ‘Lucida Grande’, ‘Lucida Sans Unicode’, Geneva, Verdana, sans-serif;">
                <span>© 2019 TECNIMUEBLES. Todos los derechos reservados.</span>
            </div>
        </td>
    </tr>
</table>
    `
    }
    return body;
}


module.exports = mailerController;