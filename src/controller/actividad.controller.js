const actividadController= {};
const mysqlConnection = require('../database/database');

actividadController.SP_actividad_insert_update = function(req, res){

    const {id_actividad,			
	titulo ,	
	descripcion ,	
	fecha_notificacion,	
	hora_inicio ,	
	hora_fin,	
    estado,
    todo_el_dia,
    id_usuario 
	} = req.body;

    const query = `call SP_actividad_insert_update(?, ?, ?, ?, ?, ?, ?, ?, ?)`;

    mysqlConnection.query(query, [id_actividad, titulo ,descripcion , fecha_notificacion, hora_inicio , hora_fin, estado, todo_el_dia, 1], (err, actividad)=>{
        if(!err){
            res.json({
                status:'success',
                code:200,
                message: actividad[0][0].var_mensaje
            })
        }
        else{
            res.json(err);
        }
    })

}

actividadController.SP_actividad_list = function(req, res){
    const query = `call SP_actividad_list()`;
    mysqlConnection.query(query, [], (err, actividad)=>{
        if(!err){
            res.json({
                status:'success',
                code:200,
                actividad: actividad[0]
            })
        }
        else{
            res.json(err);
        }
    })
}

actividadController.SP_notificaciones_list = function(req, res){
    const query = `call SP_notificaciones_list()`;
    mysqlConnection.query(query, [], (err, actividad)=>{
        if(!err){
            res.json({
                status:'success',
                code:200,
                actividad: actividad[0]
            })
        }
        else{
            res.json(err);
        }
    })
}



module.exports = actividadController;