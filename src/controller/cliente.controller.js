const clienteController= {};
const mysqlConnection = require('../database/database');

clienteController.SP_cliente_insert_update = function(req, res){
    const {id_cliente, nombre, apellidos, direccion, telefono,latitud,longitud,correo} = req.body;
    const query = `call SP_cliente_insert_update(?, ?, ?, ?, ?,?,?,?)`;
    mysqlConnection.query(query, [id_cliente, nombre, apellidos, direccion, telefono,latitud,longitud,correo], (err, cliente) => {
        if(!err){            
            res.json({
                status: 'success',
                code:200,
                message: cliente[0][0].var_mensaje
            });
        }
        else{
            res.json(err);
        }
    })
}

clienteController.SP_cliente_list = function(req, res){
    const id_cliente = req.params.id;
    const query= `call SP_cliente_list(?)`;
    mysqlConnection.query(query,[id_cliente],(err,cliente)=>{ 
        if(!err){
            res.json({
                status:'success',
                code:200,
                cliente:cliente[0]
            })
        }
        else{
            res.json(err);
        }
    })
} 

module.exports = clienteController;
