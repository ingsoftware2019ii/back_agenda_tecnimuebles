const mysql = require('mysql');

const mysqlConection = mysql.createConnection({

    
    database: 'bd_agenda',
    host: 'localhost',
    user: 'root',
    password: 'password',
    //password: 'toor',

});

mysqlConection.connect(function (err) {
    if(err) {
        console.log(err);
        return;
    } else {
        console.log('Database is connected');
    } 
});

module.exports = mysqlConection;