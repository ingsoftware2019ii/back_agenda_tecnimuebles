const express= require('express');
const router= express.Router();
const auth = require('../middlewares/authentication');
const administradorController = require('../controller/administrador.controller');

router.post('/login',administradorController.SP_admin_login);
router.post('/registrar', administradorController.SP_admin_insert_update);
router.post('/editar', administradorController.SP_admin_update);
router.post('/cambiar-estado', administradorController.SP_admin_update_estado);
router.post('/cambiar-password', administradorController.SP_admin_update_password);
router.post('/reestablecer-password', administradorController.SP_admin_send_code_password);
router.post('/enviar-codigo', administradorController.SP_admin_reset_password);
router.get('/list/:id', [auth.ensureAuth, auth.isAdmin], administradorController.SP_admin_list);
router.get('/list', administradorController.SP_admin_list_datos_propios);
router.get('/tipos', administradorController.SP_admin_tipos_list);
router.get('/reporte', auth.ensureAuth, administradorController.SP_reporte_general);

module.exports= router;