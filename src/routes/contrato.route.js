const express= require('express');
const router= express.Router();
const auth = require('../middlewares/authentication');
const contratoController = require('../controller/contrato.controller');

router.post('/registrar', contratoController.SP_contrato_insert_update);
router.get('/list/:id', contratoController.SP_contrato_list);

module.exports= router;