const express= require('express');
const router= express.Router();
const auth = require('../middlewares/authentication');
const clienteController = require('../controller/cliente.controller');

router.post('/registrar', clienteController.SP_cliente_insert_update);
router.get('/list/:id', clienteController.SP_cliente_list);

module.exports= router;