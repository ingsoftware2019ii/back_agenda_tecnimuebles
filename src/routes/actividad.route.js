const express= require('express');
const router= express.Router();
const auth = require('../middlewares/authentication');
const actividadController = require('../controller/actividad.controller');

router.post('/registrar', actividadController.SP_actividad_insert_update);
router.get('/list', actividadController.SP_actividad_list);
router.get('/ultimos' , actividadController.SP_notificaciones_list);

module.exports= router;